####Installation
Please refer to [install_instructions.md](install_instructions.md)

####Tutorial
Please refer to [tutorial](examples/narma/) for a step-by-step tutorial on how to use Spearmint in practice.

####What is this?
This is a fork of the original code for Spearmint, available [here](https://github.com/HIPS/Spearmint), specifically of the **PESM** branch. This fork mainly exists to have a working code base to refer to in our [paper](https://arxiv.org/abs/1611.05193), which uses this framework to optimize reservoir computing algorithms. The main difference is the removal of the original mongodb backend in favor of a sqlite backend, which needs no background process. Furthermore, some dependencies were updated to be compatible with the current versions of the modules that are used.

We also provide a more detailed installation procedure in [installation_instructions.md](installation_instructions.md), as well as a step-by-step [tutorial](examples/narma/) on using Spearmint, in this case the optimization of a reservoir computing algorithm.


####Original documentation:
Please refer to the original documentation for the relevant publications that went into making Spearmint: [README_old.md](README_old.md)

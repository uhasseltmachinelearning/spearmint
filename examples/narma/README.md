
# Tutorial: Hyperparameter optimization of a nonlinear delay node with Mackey-Glass on the NARMA10 benchmark using Spearmint

This is a practical example of how one would use Spearmint to optimize the hyperparameters of a problem in Reservoir computing, namely a nonlinear delay node with Mackey-Glass on the NARMA10 benchmark. We'll start off by activating the spearmint environment within anaconda, which we defined during the installation (if you haven't installed the framework yet, please refer to [install_instructions.md](../../install_instructions.md)).
```bash
source activate spearmint
```

We will install the pyyaml module for loading our parameters, and sci-kit learn for the linear regression.
```bash
conda install pyyaml
conda install scikit-learn
```
Now that we have our dependencies in order, let's start the implementation. We'll have one python file which defines a single run of the network, given the provided parameters, which returns the performance on the validation set. This is the file **narma.py**. The parameters for this file are stored in a yaml file for convenience, namely **params.yaml**.

To interface with Spearmint, we will need an additional file which defines a _main_ function that takes a job_id (which we won't be using) and a dictionary of parameters, and returns the performance for that set of parameters. This is defined in **spear.py**. There we also have some code that overrides the parameters defined in the **params.yaml** file with the ones Spearmint suggests.

The last thing to do is to configure the **config.json**, which defines the parameters Spearmint is going to tweak as well as their ranges. We will be using the ExpectedImprovement acquisition function (defined in the config.json as _acquisition_). You can give the experiment a name using the _experiment-name_ parameter. Finally, we can allow the optimization to take into account noise in the samples by setting the _likelihood_ parameters to _GAUSSIAN_. _main_file_ is the file containing the _main_ function, discussed earlier. In our case, this is the **spear.py** file. The remaining parameters in the **config.json** file are pretty self-explanatory. The _size_ parameter for the hyperparameters signifies the dimension of that parameter (as Spearmint also allows passing vectors of values); as the parameters we are considering are scalars, they have dimension 1.

Finally, we can run the optimization. To do so, from `SPEARMINT_ROOT/spearmint/` simply run:
```bash
python main.py ../examples/narma/
```
The optimization process will now start. While running Spearmint outputs some information about it's current state, e.g. the best set of hyperparameters encountered so far. All this is also being recorded in a file **main.log** in `SPEARMINT_ROOT/examples/narma/output/`. This folder also contains output files for each of the optimization steps. The state of the Gaussian process is saved to a sqlite database file after every step of the optimization, in a file called **Narma10.db**. In general this file will be named using the _experiment-name_ parameter in the **config.json** file. This allows the optimization process to continue from the last checkpoint should it be interrupted. Running `python main.py ../examples/SantaFeESN/` will then pick up from the last saved optimization point.

An example of an optimization run is given in the file **main.log** in `SPEARMINT_ROOT/examples/narma/`. The optimal set of hyperparameters is reported each step, indicated by _Minimum of observed values ..._ Taking these values and plugging them into the **params.yaml** file allows us to now test these values on a newly generated NARMA10 set. The values in **params.yaml** were obtained using a second optimization run where the range of the gamma parameter was set to a better range (0.00001 to 0.0004). This range was determined after observing the convergence of the optimization process to this range (see **example.log**. The log of this second optimization run can be found as **smaller_range.log**.

Let's have a look at the convergence of the initial optimization run (for this we used `grep -A $((3 + 2)) 'Suggestion' main.log | grep -v Minimum | grep -o '[0-9.]*' | xargs -n 3` and plotted it using matplotlib):

![convergence](figures/3Dplot.png)  
This demonstrates nicely how Spearmint discards large regions of the parameter space using only a few samples. For example, it quickly recognizes that the value of gamma needs to be very small in order to get a good performance. This can be seen more clearly if we plot a histogram of the number of samples for various values of gamma:

![histogram](figures/hist.png)

To test, simply run:
```bash
python spear.py
```
This will run the experiment with the parameters provided in **params.yaml** 100 times, printing the average and the standard deviation. Note that this overrides the _load_data_ parameter to ensure the creation of new NARMA10 sequences. For the parameters in **params.yaml** we get a result of **0.094 +- 0.04**. The large variance is caused by a few outliers, as is demonstrated here:

![performance](figures/performance.png)

The lowest value observed is **0.053** and the median is **0.082**.

To clean the working directory (i.e. remove all the output files as well as the database) you can run:
```bash
python cleanup.py ../examples/SantaFeESN/
```

Finally, a few bash commands that could come in handy when parsing the Spearmint log files.
```bash
# Get the best value at each iteration
grep 'Minimum of' main.log | grep -o '[0-9.]*'

# Get the suggested parameters (useful for plotting the convergence)
# Replace "3" with the number of parameters in your optimization
grep -A $((3 + 2)) 'Suggestion' main.log | grep -v Minimum | grep -o '[0-9.]*' | xargs -n 3
```


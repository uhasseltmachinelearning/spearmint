import numpy as np
import pylab as plt
import os
import sys
from sklearn import linear_model
from sklearn.externals import joblib
import time
import yaml

__author__ = 'Thijs Becker'
__date__ = '3/6/2016'

def get_params(params_file):
    with open(params_file, 'r') as stream:
        try:
            return yaml.load(stream)
        except:
            raise Exception('Failed to load params! %s' % (params_file))

def create_narma10(sl, sel=300):
    """
    Calculate Narma10 time series
    input: sl = sample length
           sel = sample equilibration length
    """
    cwd = os.getcwd()
    cwd_data = cwd + '/data/'
    # if the maximum is higher than 1.5, the time-series has diverged
    max_wrong = 1
    while max_wrong:
        # input is random numbers between 0 and 0.5
        u = np.random.uniform(0, 0.5, size=sel+sl+10)
        # start input is all zeros
        y = np.zeros(sel+sl+10)
        for n in range(10, sel+sl+10):
            y[n] = 0.3*y[n-1] + 0.05*y[n-1]*(y[n-1]+y[n-2]+y[n-3]+y[n-4]+y[n-5]+y[n-6]+y[n-7]+y[n-8]+y[n-9]+y[n-10]) \
                   + 1.5*u[n-10]*u[n-1] + 0.1
        if max(y) < 1.5:
            max_wrong = 0
#    np.savetxt(cwd_data + 'input.dat', u[sel+10:])
#    np.savetxt(cwd_data + 'output.dat', y[sel+10:])
    return u[sel+10:], y[sel+10:]


def compute_res(gjt, li, eta, p, theta, NumNodes, ndt):
    """
    compute the reservoir states use Euler integration
    :param gjt: gamma * J(t) is the input
    :param li: length of the input data
    :param eta, p: determines Mackey-Glass dynamics
    :param theta: node spacing
    :param NumNodes: number of nodes
    :param ndt: step size dt = theta / ndt
    :return the complete reservoir state, with one row = [x(t - (theta/N)(N - 1), x(t - (theta/N)(N-2), ..., x(t)]
    """
    def mg_der(x, xmt, gjt, eta, p):
        """
        calculate derivative predicted by Mackey-Glass oscillator
        """
        return -x + eta * ( (xmt + gjt) / (1 + pow(xmt + gjt, p)) )
    delta_t = theta / float(ndt)
    res_state = []
    # initial state: all values zero
    history = [0.0] * (NumNodes * ndt)
    state = 0.0
    for i in range(0, li):
        new_res_state = []
        new_history = []
        for j in range(0, NumNodes):
            jt = gjt[i * NumNodes + j]
            for k in range(0, ndt):
                new_history.append(state)
                state += delta_t * mg_der(state, history[j * ndt + k], jt, eta, p)
            new_res_state.append(state)
        res_state.append(new_res_state)
        history = new_history
    return res_state


def calc_sq_diff(pred_arr, target_arr):
    pred_arr, target_arr = np.asarray(pred_arr), np.asarray(target_arr)
    arr_diff = np.subtract(pred_arr, target_arr)
    arr_sq = np.square(arr_diff)
    return np.sum(arr_sq) / (float(len(arr_sq)) * np.var(target_arr))


def main_narma_params(dctn, load_data=1):

    time_start = time.time()
    # ndt: number of integration steps between two node states for the Euler integration
    ndt = 1
    p = dctn['p']
    gamma = dctn['gamma']
    eta = dctn['eta']
    theta = dctn['theta']
    offset = dctn['offset']

    cwd = os.getcwd()
    cwd_data = cwd + '/data_narma10/'

    # number of initialization steps before the measurement begins
    n_init = dctn['n_init'] 
    # number of time steps for training, validation, initialization, and prediction
    # note that one element in the test set gets discarded, since we shift the output one time-step from the input
    # (because we need to predict the next timestep)
    n_train = dctn['n_train'] + n_init
    n_val = dctn['n_val'] + n_init
    n_calc = n_init + n_train + n_val

    # create new narma10 dataset if needed
    if not dctn['load_data']:
        inp, outp = create_narma10(n_calc)
    else:
        inp, outp = np.loadtxt(cwd_data + 'input.dat'), np.loadtxt(cwd_data + 'output.dat')

    NumNodes = dctn['NumNodes']
    # calculate the input stream gamma * J(t) = gamma * I(t) * Mask
    # mask is a binary function -rn_range or rn_range
    rn_range = 1.
    gjt = []
    # We load the random numbers from disk to generate the mask
    try:
        mask_rnd = np.loadtxt(cwd_data + 'mask_%i.dat' % NumNodes)
    except:
        mask_rnd = np.random.randint(2, size=NumNodes)
        np.savetxt(cwd_data + 'mask_%i.dat' % NumNodes, mask_rnd)
        
    mask = gamma * (rn_range * (2. * mask_rnd - 1.) + offset)
    for i in range(n_calc):
        gjt.extend(np.multiply(inp[i], mask))

    # perform Euler integration to compute reservoir state
    reservoir = np.asarray(compute_res(gjt, n_calc, eta, p, theta, NumNodes, ndt))

    # shift the output one step ahead, since it needs to be predicted
    n_r = reservoir.shape[0]
    reservoir = reservoir[:n_r-1]
    outp = outp[1:]

    # remove initialisation points
    res_train = reservoir[n_init:n_train]
    outp_train = outp[n_init:n_train]
    res_val = reservoir[n_train+n_init:n_train+n_val]
    outp_val = outp[n_train+n_init:n_train+n_val]

    # delete original arrays to reduce memory consumption
    del reservoir
    del outp
    del inp

    # system is very sensitive to noise, ridge regression always worsens the performance
    alpha_ridge = 0
    clf = linear_model.Ridge(alpha=alpha_ridge, solver='svd', fit_intercept=True)
    clf.fit(res_train, outp_train)

    res_validate = clf.predict(res_val)
    val_error_sk = calc_sq_diff(res_validate, outp_val)

    time_end = time.time()
    print 'Duration: %f' % (time_end - time_start)
    print np.sqrt(val_error_sk)
    return np.sqrt(val_error_sk)

if __name__ == "__main__":
    main_narma_params(get_params('params.yaml'))

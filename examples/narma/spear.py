import narma
import numpy as np

def flatten_arrays(dict):
    new_dict = {}
    for key, value in dict.iteritems():
        try:
            if value.shape[0] == 1:
                new_dict[key] = value[0]
        except:
            new_dict[key] = value
    return new_dict

def run(params):
    pr_dict = narma.get_params('params.yaml')
    fl_params = flatten_arrays(params)
    # Override the default params
    for param in fl_params:
        pr_dict[param] = fl_params[param]
    return narma.main_narma_params(dctn=pr_dict, load_data=0)

def main(job_id, params):
    return run(params)

if __name__ == '__main__':
    results = []
    for i in range(100):
        results.append(run({'load_data': False}))
    print 'Test: %f +- %f' % (np.average(results), np.std(results)) 


# Install Spearmint on clean Ubuntu 16.04 install
In this guide I'll explain how to install this framework (which is a fork of [Spearmint](https://github.com/HIPS/Spearmint)) on a clean install of Ubuntu 16.04. We'll be using [Anaconda](https://www.continuum.io/downloads) as this simplifies the install process substantially.  Anaconda (or Miniconda, should you prefer) can be installed as a user.

First, install Anaconda by downloading the Python 2.7 version of the bash script on their [website](https://www.continuum.io/downloads). Once downloaded, install it by simply running the script and following the on-screen instructions. Assuming the script was downloaded to ~/Downloads/ you would use e.g.: 
```
bash ~/Downloads/Anaconda2-4.4.0-Linux-x86_64.sh
```

While installing Anaconda will ask to modify your ~/.bashrc file, which is what we want. Just to make sure your environment variables are updated to use the conda installation, restart your terminal or use:
```
source ~/.bashrc
```

Next, we'll create a new virtual environment specifically for Spearmint. We can do this using:
```
conda create --name spearmint
```
Or some other name you want. Now activate the environment using:
```
source activate spearmint
```
Now we'll start installing the required Python modules:
```
conda install -c ccordoba12 pygmo=1.1.5
conda install numpy scipy matplotlib sqlite
conda install -c conda-forge weave
conda install -c anaconda gcc=4.8.5
conda install -c conda-forge nlopt=2.4.2
```
Note the specific version for **gcc**. I included this as version 5.4.0 that ships with Ubuntu 16.04 caused problems for me.

Now all that's left is cloning this repository and compiling it. If you don't have **git** installed yet (which is the case for a clean Ubuntu install), simply use `sudo apt-get install git` to get it.

We will create a new folder for git projects, after which we will clone the repository. Make sure you switch to the **cleanInstall** branch.
```
cd ~
mkdir git
cd git
git clone https://JanYperman@bitbucket.org/uhasseltmachinelearning/spearmint.git
cd spearmint
git checkout cleanInstall
```
All that's left now is to install Spearmint:
```
cd ~/git/spearmint/
python setup.py install
```
This should finish successfully. To test the installation, you might want to run one of the provided examples by running:
```
cd ~/git/spearmint/spearmint
python main.py ../examples/simple
```
Spearmint should now begin optimizing the *branin* example.

For a step-by-step tutorial of how to use the spearmint library, please refer to the [tutorial](examples/narma/).